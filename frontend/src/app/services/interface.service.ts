import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class InterfaceService {

  readonly URL = 'http://localhost:5000/';
  constructor(private http: HttpClient) { }

  getImagen(){   
    return this.http.get(this.URL+'sesion', {responseType: "blob"});
  }

  getMensaje(){   
    return this.http.get(this.URL + 'what/getMensaje');
  }

  setMensaje(mensajeCompleto){   
    return this.http.post(this.URL+'what/setMensaje/', mensajeCompleto);
  }

  postFile(fileToUpload: File)/*: Observable<boolean>*/ {
    const formData =  new FormData();
    formData.append('file', fileToUpload);
    return this.http.post(this.URL+'what/subexcel', formData);
    //     // do something, if upload success
    //   console.log(data);
    // }, error => {
    //   console.log(error);
    // });
    //.catch((e) => console.log(e));
  }
  sendMensajesWhatsapp(datos_numeros){
     return this.http.post(this.URL + 'what/sendmsj/', datos_numeros);
  }

  
}
