import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { User } from '../models/user';
//import {config} from '../config/config';
 
@Injectable()
export class UserService {
  //selectedNotes: Notes;
  users: User[];
  
  readonly URL = 'http://localhost:5000/what/user/';
  constructor(private http: HttpClient) { }
 

  registerUser(user : User){
    console.log(user);
    //return this.http.post(this.URL_API + '/crear/', user)
    return this.http.post(this.URL + 'crear/', user);
    
       
  }
  getUsers(){
    var my_token =localStorage.getItem('auth_token');    
    return this.http.get(this.URL+'', {headers: new HttpHeaders(
      {
        'Authorization': `Bearer ${my_token}`,
        'Content-Type': 'application/json'
      })});
    
  }
  borrar_usuario(_id: string){
    var my_token =localStorage.getItem('auth_token'); 
    return this.http.delete(this.URL+`${_id}`, {headers: new HttpHeaders(
      {
        'Authorization': `Bearer ${my_token}`,
        'Content-Type': 'application/json'
      })
    }); 
  }
  reactivar_usuario(_id: string){
    var my_token =localStorage.getItem('auth_token'); 
    return this.http.get(this.URL+'reactivar/'+`${_id}`, {headers: new HttpHeaders(
      {
        'Authorization': `Bearer ${my_token}`,
        'Content-Type': 'application/json'
      })
    }); 
  }
  getUser(_id: string){
    //console.log(this.URL+`${_id}`);
    var my_token =localStorage.getItem('auth_token'); 
    return this.http.get(this.URL+`${_id}`, {headers: new HttpHeaders(
      {
        'Authorization': `Bearer ${my_token}`,
        'Content-Type': 'application/json'
      })
    });
  }
  getUserFiltros(datos_filtro_usuario){
    var my_token =localStorage.getItem('auth_token'); 
     return this.http.post(this.URL + 'filtros/', datos_filtro_usuario, {headers: new HttpHeaders(
      {
        'Authorization': `Bearer ${my_token}`,
        'Content-Type': 'application/json'
      })
    });
   }
  putUser(user: User) {
    var my_token =localStorage.getItem('auth_token'); 
    return this.http.put(this.URL, user, {headers: new HttpHeaders(
      {
        'Authorization': `Bearer ${my_token}`,
        'Content-Type': 'application/json'
      })
    });
  }
  putPassword(editar_password) {
    //var my_token =localStorage.getItem('auth_token'); 
    return this.http.put(this.URL+'password', editar_password);
    // {headers: new HttpHeaders(
    //   {
    //     'Authorization': `Bearer ${my_token}`,
    //     'Content-Type': 'application/json'
    //   })
    // }
  }

  putPermisosUser(user: User) {
    var my_token =localStorage.getItem('auth_token'); 
    return this.http.put(this.URL+'permisos/', user, {headers: new HttpHeaders(
      {
        'Authorization': `Bearer ${my_token}`,
        'Content-Type': 'application/json'
      })
    });
  }

 
}

