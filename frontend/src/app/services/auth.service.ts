import { Injectable } from '@angular/core';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';
//import {config} from '../../app/config/config';
declare var M: any;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  uri = 'http://localhost:5000/what';
  token;

  constructor(private http: HttpClient,private router: Router) { }

  
    login(nombre_usuario: string, password: string) {
      this.http.post(this.uri + '/user', {nombre_usuario: nombre_usuario, password: password})
      .subscribe((resp: any) => {
      //console.log(resp);
        if(resp.status === 'OK'){
          localStorage.setItem('auth_token', resp.token);
          localStorage.setItem('exp', resp.exp);
          localStorage.setItem('_id', resp.signed_user[0]._id);
          //console.log(localStorage.getItem('_id'));
          //console.log(resp);
          let user_string = JSON.stringify({nombre_usuario, password});
          localStorage.setItem('currentUser', user_string);
          //console.log('ok')
          this.router.navigate(['home']);
          
        }else{
          //console.log('M: error. '+resp);
          M.toast({html: 'Usuario y/o contraseña incorrecta'});
        }
    });
       
    }
    public getlogIn(): boolean {
      var retorno = false;
      var exp = localStorage.getItem('exp');
      if(localStorage.getItem('auth_token') /*!== null && (Date.now() <  (Number(exp) * 1000))*/){ 
        //  console.log('si '+Date.now() +' < '+  (Number(exp) * 1000));
          retorno=true;
      }else{
       // console.log('no '+Date.now() +' < '+  (Number(exp) * 1000));
        retorno = false;
      }
      return (retorno);
    }
    logout(){
      localStorage.removeItem('auth_token');
      localStorage.removeItem('exp');
      localStorage.removeItem('currentUser');
      localStorage.removeItem('test');
      this.router.navigate(['../user/login']);
    }
    
}
