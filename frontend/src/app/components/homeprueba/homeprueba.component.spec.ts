import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepruebaComponent } from './homeprueba.component';

describe('HomepruebaComponent', () => {
  let component: HomepruebaComponent;
  let fixture: ComponentFixture<HomepruebaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepruebaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepruebaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
