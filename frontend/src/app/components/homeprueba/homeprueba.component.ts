import { Component, OnInit } from '@angular/core';
import {InterfaceService} from '../../services/interface.service'

@Component({
  selector: 'app-homeprueba',
  templateUrl: './homeprueba.component.html',
  styleUrls: ['./homeprueba.component.css'],
  providers: [InterfaceService]
})
export class HomepruebaComponent implements OnInit {

  constructor(
    private interfaceService:InterfaceService
  ) { }

  ngOnInit() {
  }

  solicitar_imagen(){
    alert('Button');
    this.interfaceService.getImagen()
      .subscribe(res =>{       
        console.log(res);
        var imagen = document.getElementById('idqrwpp') as HTMLImageElement;
        const blobUrl = URL.createObjectURL(res)
        imagen.src = blobUrl
        //document.getElementById('cuerpo').appendChild(res.);

      }, (err) => {

      });
  }

}
