import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var M: any;

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private userService : UserService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.credenciales();
  }
  credenciales(){
    if(!this.authService.getlogIn()){
      //console.log('llego');
      this.router.navigate(['../user/login']);
      return this.authService.getlogIn();
    }
  }
  guardar_contrasenia(contra){
    //alert(contra.value);
    //alert(this.route.snapshot.queryParamMap.get('id'))
    var editar_password ={
      password : contra.value,
      _id:localStorage.getItem('_id')
    }
    this.userService.putPassword(editar_password).subscribe((respuesta)=>{
      M.toast({html: respuesta});
    });
    //console.log(contra)
  }
}
