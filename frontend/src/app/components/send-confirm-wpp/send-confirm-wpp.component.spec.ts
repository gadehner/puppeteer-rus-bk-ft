import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendConfirmWppComponent } from './send-confirm-wpp.component';

describe('SendConfirmWppComponent', () => {
  let component: SendConfirmWppComponent;
  let fixture: ComponentFixture<SendConfirmWppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendConfirmWppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendConfirmWppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
