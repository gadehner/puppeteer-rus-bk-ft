import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappsendComponent } from './whatsappsend.component';

describe('WhatsappsendComponent', () => {
  let component: WhatsappsendComponent;
  let fixture: ComponentFixture<WhatsappsendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappsendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappsendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
