import { Component, OnInit } from '@angular/core';
import {InterfaceService} from '../../services/interface.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-whatsappsend',
  templateUrl: './whatsappsend.component.html',
  styleUrls: ['./whatsappsend.component.css']
})
export class WhatsappsendComponent implements OnInit {
  provisorio = null;
  siguiente = true;
  constructor(
    private interfaceService: InterfaceService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.credenciales();
  }

  credenciales(){    
    if(!this.authService.getlogIn()){
      //console.log('llego');
      this.router.navigate(['../user/login']);
      return this.authService.getlogIn();
    }
  }

  solicitar_imagen(){
    //alert('Button');
    this.provisorio = true;
    this.interfaceService.getImagen()
      .subscribe(res =>{       
        console.log(res);
        var imagen = document.getElementById('idqrwpp') as HTMLImageElement;
        const blobUrl = URL.createObjectURL(res);
        imagen.src = blobUrl;
        this.provisorio = false;
        this.siguiente = false;

        //document.getElementById('cuerpo').appendChild(res.);

      }, (err) => {

      });
  }

}
