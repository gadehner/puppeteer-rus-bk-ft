import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {InterfaceService} from '../../services/interface.service';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { variable } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';
import {AuthService} from '../../services/auth.service';
//import {UploadexcelComponent} from './uploadexcel.component.spec';

@Component({
  selector: 'app-uploadexcel',
  templateUrl: './uploadexcel.component.html',
  styleUrls: ['./uploadexcel.component.css']
})
export class UploadexcelComponent implements OnInit {
  // displayedColumns: string[] = ['pos',	'nom', 'pol',	'tel',	'ope'];
  displayedColumns: string[] = ['pos','nom', 'pol',	'tel', 'ope'];
  dataSource= null;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  provisorio = false;
  numero: string;
  id: string;

  fileToUpload: File = null;
  nros_json = [];
  reestablecer = [];
  
  constructor(
    private interfaceService: InterfaceService,
    private http: HttpClient,
    public dialog: MatDialog,
    public dialogMessage: MatDialog,
    private router: Router,
    private spinner: NgxSpinnerService,
    private authService: AuthService
    
    
  ) { }

  ngOnInit() {
   this.credenciales();
  }
  credenciales(){
    if(!this.authService.getlogIn()){
      //console.log('llego');
      this.router.navigate(['../user/login']);
      return this.authService.getlogIn();
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    if(this.fileToUpload!==null){
      //console.log(this.fileToUpload);
      this.uploadFileToActivity();
    }else{
      this.nros_json = [];
      this.dataSource = new MatTableDataSource(this.nros_json);
    }
  }
  uploadFileToActivity() {
      this.provisorio = true;
      this.interfaceService.postFile(this.fileToUpload).subscribe(data => {
        // console.log(data);
       
        this.nros_json = data as [];
        //this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource = new MatTableDataSource(this.nros_json);
       // console.log(this.dataSource);
      //  console.log(this.nros_json);
        this.dataSource.paginator = this.paginator;
        //this.reestablecer = data as [];
        localStorage.setItem('reestablecer', JSON.stringify(this.nros_json));
        this.provisorio = false;
        }, error => {
          console.log(error);
      });
  }
  enviar_datos(){
    localStorage.setItem('reestablecer', JSON.stringify(this.nros_json));
    //console.log(JSON.parse(localStorage.getItem('reestablecer')));
    this.provisorio = true;
    
    var datos_clientes = JSON.parse(localStorage.getItem('reestablecer'));
    // var datos_clientes = {
    //   uno: 'asdasd',
    //   dos: 'jajaj'
    // };

    this.detailsMessage(datos_clientes);

    
    //this.router.navigate(['send-confirm']);
  }
  details(id_json) {
    id_json = id_json-1;
    this.numero=this.nros_json[id_json].telefono;
    const dialogRef = this.dialog.open(DialogDetails, {
      width: '250px',
      data: {numero: this.numero, id:id_json}
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('Cerrar Dialogo');
      this.numero= result;
      //console.log('Num: '+this.numero);
      if(String(this.numero)!=='undefined'){
        //this.reestablecer[id_json].telefono=this.nros_json[id_json].telefono;
        this.nros_json[id_json].telefono= this.numero;
        this.dataSource = new MatTableDataSource(this.nros_json);
        this.dataSource.paginator = this.paginator;
        //console.log(this.nros_json[4]);
      }
    });
  }
  reestablecer_num(id_json){
    id_json = id_json-1;
    // console.log('llego');
    // console.log(JSON.parse(localStorage.getItem('reestablecer'))[id_json]);
    // console.log(this.nros_json[id_json]);
    this.nros_json[id_json].telefono = JSON.parse(localStorage.getItem('reestablecer'))[id_json].telefono;
    this.dataSource = new MatTableDataSource(this.nros_json);
    this.dataSource.paginator = this.paginator;
  }

  detailsMessage(datos_clientes) {
    var direccion;
    var mensaje;
    var mensajeVisual;
    var firma;
    var mensajeInput;

    this.interfaceService.getMensaje().subscribe(respuesta => {
      //console.log(respuesta);
      var array = [];
      respuesta = String(respuesta).replace(/\n/g, "");
      respuesta = String(respuesta).replace(/\*/g, "");
      respuesta = String(respuesta).replace(/_/g, "");
      array= String(respuesta).split('%0A');
      //console.log(array);
      direccion = array[1];
      firma = array[array.length-1];
      mensaje = array;
      mensaje.splice(0, 2);
      mensaje.splice(mensaje.length-1, 1);
      mensajeVisual = mensaje;

      mensajeInput = String(mensaje);
      // mensajeInput = mensajeInput.replace(/,/g, "\n");
      mensajeInput = mensajeInput.replace(/,/g, "\n");



      const dialogRef = this.dialogMessage.open(DialogDetailsMessage, {
        width: '800px',
        data: {direccion: direccion, message: mensajeInput, firma:firma, messageVisual:mensajeVisual/*.split('<br>')*/, guardar:'NO'}
      });
      dialogRef.afterClosed().subscribe(result => {
        //console.log(result);
        if(String(result)!=='undefined'){
          // console.log(direccion);
          // console.log(message);
          // console.log(firma);
          this.spinner.show();

          var cuerpoCompleto='%0A_';
          cuerpoCompleto += result.message.replace(/\n/g, "_\n%0A_");
          cuerpoCompleto += '_';
          
          var mensajeCompleto='%0A*_'+result.direccion+'_*\n' + cuerpoCompleto + '\n' + '%0A*'+result.firma+'*'
          // console.log(result);
          this.interfaceService.setMensaje(({mensaje:mensajeCompleto})).subscribe(respuesta=>{
            var usuario_productor = JSON.parse(localStorage.getItem('currentUser')).nombre_usuario;
            var datos_clientes_productor ={
              datos_clientes:datos_clientes,
              usuario_productor:usuario_productor
            }
            this.interfaceService.sendMensajesWhatsapp(datos_clientes_productor).subscribe(data=>{
              //console.log(data);
              var respuesta = Object(data);
              //console.log(respuesta);
              // this.nros_json = data as [];
              // this.dataSource = new MatTableDataSource(this.nros_json);
        
              // this.dataSource.paginator = this.paginator;
              // localStorage.setItem('reestablecer', JSON.stringify(this.nros_json));
/**
 * telefono_vacio:0,
        con_telefono_deshabilitado:0,
        misma_fecha:0,
        enviado:0,
        no_tiene_wpp:0
 */

              this.provisorio = false;
              Swal.fire(
                
                'Perfecto!',
                'El proceso se complet&oacute; correctamente. '+ 
                '<div style="margin:0px; padding:0px; text-align: left; background-color: #B9FFA3"><strong> Enviados Correctamente: '+ respuesta.enviado +'</strong></div>'+
                '<div style="margin:0px; padding:0px; text-align: left; background-color: #FFC0A3"> No enviados | Sin Whatsapp: '+ respuesta.no_tiene_wpp +
                '<br> No enviados | Sin tel&eacute;fono: '+ respuesta.telefono_vacio +
                '<br> No enviados | Deshabilitados manualmente: '+ respuesta.con_telefono_deshabilitado +
                '<br> No enviados | Ya enviado en este d&iacute;a: '+respuesta.misma_fecha +'</div>'+
                '<br><strong> Total: '+(respuesta.enviado+respuesta.no_tiene_wpp+respuesta.telefono_vacio+respuesta.con_telefono_deshabilitado+respuesta.misma_fecha) +'</strong>',
                'success'
              )
              this.spinner.hide();
              

            })

          })

        }else{
          this.provisorio = false;
          Swal.fire('Oops...', 'Los mensajes no fueron enviados!', 'error');
          //alert('Los mensajes no fueron enviados');
        }


      });
    
    
    });

    
    
  }



}

// Componente para editar numero de telefono
@Component({
  selector: 'dialog-details.component',
  templateUrl: 'dialog-details.component.html',
})
export class DialogDetails {
  panelOpenState = false;
  constructor(
    public dialogRef: MatDialogRef<DialogDetails>,
    @Inject(MAT_DIALOG_DATA) public data: String) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

// Componente para editar mensaje a enviar
@Component({
  selector: 'dialog-details-message.component',
  templateUrl: 'dialog-details-message.component.html',
  styleUrls: ['./dialog-details-message.component.css']
})
export class DialogDetailsMessage {
  panelOpenState = false;
  constructor(
    public dialogRef: MatDialogRef<DialogDetailsMessage>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  save(){
    this.data.guardar ='OK';
  }
  cambio_visual(mj){
    
    //mjv = mj.replace(/\n/g, "<br/>");
    this.data.messageVisual = mj.replace(/\n/g, "<br>");
    this.data.messageVisual = this.data.messageVisual.split('<br>');
    //console.log(this.data.messageVisual);
    //console.log(mjv)
  }
}