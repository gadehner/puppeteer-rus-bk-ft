import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.credenciales();
  }
  credenciales(){
    if(!this.authService.getlogIn()){
      this.router.navigate(['../user/login']);
      return this.authService.getlogIn();
    }
  }
}
