import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
   
  constructor(private authService: AuthService, private router: Router) {
     
  }
  login(UserName: string, Password: string) {
    console.log("you are logging in")
    this.username = UserName;
    this.password = Password;
    this.authService.login(this.username, this.password)
   
  }
 
  ngOnInit() { 
    //this.credenciales();
  }

  credenciales(){
    //localStorage.removeItem('auth_token')
    //console.log(localStorage.getItem('auth_token'));
    //console.log(localStorage.getItem('currentUser'));
    
    /*if(String(localStorage.getItem('auth_token')).length>4){
      //console.log('no null');
      this.router.navigate(['../home']);
      return true;
    }else{
      //console.log('null');
      this.router.navigate(['../user/login']);
      return false;
    }*/
    this.router.navigate(['../home']);
      return true;
  }
}
