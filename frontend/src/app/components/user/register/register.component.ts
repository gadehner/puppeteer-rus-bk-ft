import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';
import { NgForm } from '@angular/forms';
import { UserService } from '../../../services/user.service';
//import { ToastrService } from 'ngx-toastr'
 
declare var M: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]
})

export class RegisterComponent implements OnInit {
  user: User;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
 
  constructor(private userService: UserService/*, private toastr: ToastrService*/) { }
 
  ngOnInit() {
    this.resetForm();
  }
 
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.user = {
      nombre_usuario: '',
      password: '',
      email: '',
      nombre: '',
      apellido: '',
      domicilio:'',
      telefono:'',
      rol:'',
      estado:''
    }
  }
 
  OnSubmit(form: NgForm) {
    this.userService.registerUser(form.value)
    .subscribe((resp: any) => {
      if(resp.status=='Usuario guardado'){//ver desp
        console.log('volvio');
        console.log(resp);
        M.toast({html: 'Registrado correctamente'});
        this.resetForm(form);
      }else{
        M.toast({html: resp.status + '. Ingrese los datos correctamente'});
      }
    
    });
      
        
      
      
  }
 
}

/**
 * @Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
 */