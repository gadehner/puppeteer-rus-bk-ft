import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomepruebaComponent } from './components/homeprueba/homeprueba.component';

import { InterfaceService } from './services/interface.service';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import {HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { UserComponent } from './components/user/user.component';
import { Page404Component } from './components/page404/page404.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/user/login/login.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { RegisterComponent } from './components/user/register/register.component';
import { WhatsappsendComponent } from './components/whatsappsend/whatsappsend.component';
import { UploadexcelComponent } from './components/uploadexcel/uploadexcel.component';

import {MatDialogModule} from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
//import {} from './components/uploadexcel/';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule
} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import { MatTabsModule, MatTooltipModule, MatExpansionModule, MatProgressBarModule, MatListModule, MatTableModule, MatPaginatorModule, MatSelectModule, MatCheckbox, MatCheckboxModule, MatCardModule, MatIconModule } from '@angular/material';

import {DialogDetails} from './components/uploadexcel/uploadexcel.component';
import {DialogDetailsMessage} from './components/uploadexcel/uploadexcel.component';
import { SendConfirmWppComponent } from './components/send-confirm-wpp/send-confirm-wpp.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { EditUserComponent } from './components/edit-user/edit-user.component';


@NgModule({
  declarations: [
    AppComponent,
    HomepruebaComponent,
    UserComponent,
    Page404Component,
    NavbarComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    WhatsappsendComponent,
    UploadexcelComponent,
    DialogDetails,
    DialogDetailsMessage,
    SendConfirmWppComponent,
    EditUserComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTabsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatDialogModule,
    MatTooltipModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    NgxSpinnerModule,
    
  
  ],
  exports:[
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTooltipModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    

    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatDialogModule,
    MatTooltipModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    DialogDetails,
    DialogDetailsMessage
  ],
  providers: [InterfaceService, AuthService, UserService],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogDetails,
    DialogDetailsMessage]

})
export class AppModule { }
