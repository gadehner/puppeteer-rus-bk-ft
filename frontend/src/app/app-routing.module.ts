import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/user/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { Page404Component } from './components/page404/page404.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { RegisterComponent } from './components/user/register/register.component';
import { UserComponent } from './components/user/user.component';
import {WhatsappsendComponent} from './components/whatsappsend/whatsappsend.component';
import {UploadexcelComponent} from './components/uploadexcel/uploadexcel.component';
import {SendConfirmWppComponent} from './components/send-confirm-wpp/send-confirm-wpp.component';
import {EditUserComponent} from './components/edit-user/edit-user.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  
  {path: 'home', component: HomeComponent},

  {
    path: 'whatsappsend', component: WhatsappsendComponent,
   // children: [{ path: '', component: UploadexcelComponent }]
  },//
  {path: 'uploadexcel', component: UploadexcelComponent},
  //{path: 'uploadexcel', component: UploadexcelComponent},
  {path: 'send-confirm', component: SendConfirmWppComponent},
  
  {path: 'user/profile', component: ProfileComponent},//solo us autentificados
  {
    path: 'user/signup', component: UserComponent,
    children: [{ path: '', component: RegisterComponent }]
  },
  {
      path: 'user/login', component: UserComponent,
      children: [{ path: '', component: LoginComponent }]
  },
  {path: 'user/edit_password', component: EditUserComponent},
  
  {path: '**', component: Page404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
