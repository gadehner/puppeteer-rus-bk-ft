export class User {
    email: string;
    nombre_usuario: string;
    nombre: string;
    apellido: string;
    password: string;
    telefono: string;
    domicilio: string;
    rol: string;
    estado: string;
}

