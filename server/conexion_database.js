const mongoose = require('mongoose');
const config = require('./config');
const URI = config.URI_MONGO+config.NOMBRE_BD_MONGO;

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

mongoose.connect(URI,{useUnifiedTopology: true, useNewUrlParser: true})
    .then(db => console.log('Base de datos conectada en '+ URI))
    .catch(err => console.error(err));
    
module.exports = mongoose;