const excel_to_json = {};
const excelToJson = require('convert-excel-to-json');
//https://github.com/sarfraznawaz2005/whatspup
 
/*const result = excelToJson({
    sourceFile: '/home/gabrieldehner/locales/sistema-rus-mensajes/archivos-prueba/polizasVencidas.xls'
}).listadoPolizas;
console.log(result);*/
//pacienteController.createPaciente = async (req, res) => {
excel_to_json.obtener_numeros_excel = (ruta_archivo)=>{

  const result = excelToJson({
    sourceFile: __dirname+'/../'+ruta_archivo
  });
  var valor='polizasFUERACoberturaTOTAL-PROD';

  var array_polizas_sin_cobertura = result[valor];
  var array_datos=[];
  for(i = 1; i<array_polizas_sin_cobertura.length; i++ ){
    var telefono = array_polizas_sin_cobertura[i]['H'];
    var poliza = array_polizas_sin_cobertura[i]['B'];
    var nombre = array_polizas_sin_cobertura[i]['F']+' '+array_polizas_sin_cobertura[i]['G'];
    telefono = String(telefono);
    telefono = replaceAll(telefono, '  ', ' ');
    var index = telefono.lastIndexOf('Celular:'); //8letras +54376436294888 //15letras
    //tmb se deben contemplar los telefonos particulares pq algunos tienen cargado ahi.
    var paso = false;
    var permitido = true;
    if(index!==-1){
      
      // var ant =telefono;

      telefono = telefono.substring(index, index+8+15+1);
      telefono = telefono.split('Celular:')[1];
      telefono = telefono.replace(' ','');
      telefono = telefono.split(' ')[0];
      telefono = replaceAll(telefono, ' ', '');
      telefono = replaceAll(telefono, '-', '');
      telefono = replaceAll(telefono, ':', '');
      telefono = telefono.replace(/[a-zA-Z.]+/, '');


      // telefono.indexOf('154') &&
      //si al principio hay un 15, se lo reemplaza por 376
      if(telefono.indexOf('0')==0){
        telefono = telefono.replace('0','');
      }
      // if(telefono.indexOf('15')==0){
      //   telefono = telefono.replace('15','376');
      //   //ver si la cantidad de numeros es asd entonces3764, etc etc
      //   //conviene partir los numeros
      // }
      
      // var texto='*_Estimado Cliente: _*'+
      // ' %0A_Le informamos que su póliza de seguro se encuentra impaga a la fecha._'+
      // ' %0A_Le sugerimos regularice su situación para evitar inconvenientes._'+
      // ' %0A_SI YA ABONÓ DEJE SIN EFECTO ESTE MENSAJE._'+
      // ' %0A_Saludos._'+
      // ' %0A*Atte: Roberto Dehner | Río Uruguay Seguros.*'
      // var link ='https://web.whatsapp.com/send?phone='+'+54'+telefono+'&text='+texto;
      // console.log(link);
      paso=true;
      telefono= ''+telefono;
      var numero_area_provincia = telefono.substring(0,telefono.length-6);
      if(numero_area_provincia.indexOf('15')==0 && numero_area_provincia.length==3){
        numero_area_provincia = numero_area_provincia.replace('15','376');
      }else{
        if(numero_area_provincia.indexOf('15')==0 && numero_area_provincia.length==2){
          numero_area_provincia = numero_area_provincia.replace('15','3764');
        }else{
          if(numero_area_provincia.substring(numero_area_provincia.length-2, numero_area_provincia.length ).indexOf('15')==0 ||
          numero_area_provincia.substring(numero_area_provincia.length-3, numero_area_provincia.length ).indexOf('15')==0){
            numero_area_provincia = numero_area_provincia.replace('154','');
            numero_area_provincia = numero_area_provincia.replace('155','');
            numero_area_provincia = numero_area_provincia.replace('15','');
          }else{
            if(numero_area_provincia.length==3){
              numero_area_provincia = '3'+numero_area_provincia;
            }
          }
        }
      }
      var id_pais = '54';
      var identificacion = telefono.substring(telefono.length-6,telefono.length);
      telefono= id_pais+ numero_area_provincia + identificacion;
      
    }
    if(!paso){
      telefono = '0';
      permitido= false;
    }
    var datos_socio = new Object({
      posicion: i,
      telefono: telefono,
      nombre: nombre,
      poliza: poliza,
      permitido: permitido
    })
    array_datos[array_datos.length] = datos_socio;
     //console.log(telefono);
  }
  //console.log(array_datos);
  return array_datos;
}

function replaceAll ( text, busca, reemplaza ) {
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}
//start();
module.exports = excel_to_json;