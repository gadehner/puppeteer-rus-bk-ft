const mongoose = require('mongoose');
const {Schema} = mongoose;

const DiaMensajeSchema = new Schema({
  productor: String,
  dia: String,
  datos_dia_mensaje: []
});

module.exports = mongoose.model('DiaMensaje', DiaMensajeSchema, 'DiaMensaje');