const mongoose = require('mongoose');
const {Schema} = mongoose;

const UserSchema = new Schema({
  email: {type: String, unique: true, lowercase: true },
  nombre_usuario: {type: String, unique: true},
  nombre: String,
  apellido: String,
  password: String,
  telefono: String,
  domicilio: String
});

module.exports = mongoose.model('User', UserSchema, 'usuarios_sesion');