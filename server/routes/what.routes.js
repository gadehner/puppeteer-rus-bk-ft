const express = require('express');
const router = express.Router();
const what = require('../controllers/what.controller');
const user = require('../controllers/usuarios_sesion.controller');


var multer = require('multer');
const path =  require('path');

const PATH = './subidas';

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, PATH);
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
});
var upload = multer({storage});

router.post('/subexcel', upload.single('file'), what.obtener_numeros);
router.post('/sendmsj/', what.enviar_mensaje);
router.get('/getMensaje/', what.obtener_mensaje_envio);
router.post('/setMensaje/', what.setear_mensaje_envio);

/**
 * Rutas usuario
 */

router.post('/user/crear/', user.createUser);
router.post('/user/', user.login);
router.put('/user/password/', user.editarPassword);
// router.post('/user/filtros/', auth, user.buscarFiltros);
// router.get('/user/', auth, user.getUsers);
// // router.get('/auth/', auth, user.getUsersA);
// router.get('/user/:id', auth, user.getUser);
// router.delete('/user/:id', auth, user.deleteUser);
// router.get('/user/reactivar/:id', auth, user.reactiveUser);
// router.put('/user/', auth, user.editUser);
// router.put('/user/permisos/', auth, user.permisosUser);




// router.post('/crear/', user.createUser);
// router.post('/', user.login);
// router.get('/', auth, user.getUsers);

module.exports = router;