var por_milisegundos = 1000;
var por_segundos = 60;
var por_minutos = 20;

module.exports = {
    PORT_FRONTEND: '4200',
    PORT_BACKEND: '3000',
    IP: 'localhost',
    TIME_OUT: por_milisegundos* por_segundos * por_minutos,
    TIME_OUT_2: por_milisegundos* por_segundos * por_minutos,
    NOMBRE_BD_MONGO:'bd_mensajes_dyh',
    URI_MONGO:'mongodb://localhost/',
    KEY: 'OJqQwwUg9mVgYb_9565',//'your_secret_key',
    CANTIDAD_HORAS: 5
}  

