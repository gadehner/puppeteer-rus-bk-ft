const User = require('../models/usuario_sesion'); //para consultas a BD
const usuarioController = {};
const jwt = require('jsonwebtoken');
const JWT_Secret = 'clavesecreta';
const mongoose = require('mongoose');


usuarioController.createUser = async (req, res) => {
    const user = new User({
        email: req.body.email,
        nombre_usuario: req.body.nombre_usuario,
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        password: req.body.password,
        telefono: req.body.telefono,
        domicilio: req.body.domicilio
    });
    console.log(user);
    console.log('Registro User: ' + req.body.nombre_usuario);
    var usaux = await User.find({nombre_usuario: user.nombre_usuario});
    if(usaux.length <= 0){
        console.log('email:  '+ user.email);
        usaux = await User.find({email: user.email});
        if(usaux.length <= 0){
            await user.save();
            res.json({
            'status': 'Usuario guardado'
            });
        }
    }else{
        res.json({
            'status': 'Error: Usuario ya existente'
        });
    }

};
    

usuarioController.login = async (req, res) => {
    if (req.body) {
        var user = req.body;
        console.log(user);
        console.log('Login');
        
        const usaux = await User.find({nombre_usuario: user.nombre_usuario});
        console.log(usaux);
        console.log(usaux.length);
        if(usaux.length === 1){//esto deberia usarlo en todas las paginas, como si fuera de sesion
            if (usaux[0].nombre_usuario ==user.nombre_usuario && usaux[0].password  == user.password) {
                var cantidad_horas = 1;//config.CANTIDAD_HORAS;
                var token = jwt.sign(user, JWT_Secret, { expiresIn: 60*60*cantidad_horas },{ algorithm: 'RS256'});               //{ expiresIn: 10 }
                res.json({
                    'signed_user': usaux,
                    'token': token,
                    'status': 'OK',
                    'exp': jwt.decode(token).exp,
                    'iat': jwt.decode(token).iat
                });
            } else {
                res.json({
                    'errorMessage': 'Authorization required!'
                });
            }
    
        }else{
            res.json({
                'status': 'Error: Usuario no registrado'
              });
        }


        
    } else {
        res.json({
            'errorMessage': 'Por favor, ingrese usuario y contrasenia'
        });
    }
};
usuarioController.editarPassword = async (req, res) =>{
    await User.updateOne({_id:req.body._id},{$set:{password: req.body.password}})
    console.log(req.body);
    res.json('Editado Correctamente');
}
module.exports = usuarioController;