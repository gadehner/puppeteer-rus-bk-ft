const whatController = {};
const excel_to_json = require('../services/excel_to_json');
var request = require('request');
var moment = require('moment');
const fs = require('fs');
const DiaMensaje = require('../models/dia_mensaje');
const mongoose = require('mongoose');

whatController.obtener_numeros = async (req, res) => {
    console.log(`Storage location is ${req.hostname}/${req.file.path}`);
    console.log('llego subexcel');
    console.log(req.file.path);
    //console.log(excel_to_json.obtener_numeros_excel(''+''+req.file.path))
    var datos_socios = (excel_to_json.obtener_numeros_excel(''+''+req.file.path));
    return res.json(datos_socios);
};
whatController.enviar_mensaje = async (req, res) => {
    console.log(req.body);
   
    const datos_clientes = req.body.datos_clientes;
    console.log('llegooo enviar msj');
    console.log(datos_clientes);
    //console.log(req.body);
    var errores =[];
    var dia = moment().format("MMM Do YY");
    var productor = String(req.body.usuario_productor);
    var dia_mensaje = await DiaMensaje.find({productor :productor, dia:dia});
    var id_dia;
    var retorno_resultado_proceso = new Object({
        telefono_vacio:0,
        con_telefono_deshabilitado:0,
        misma_fecha:0,
        enviado:0,
        no_tiene_wpp:0

    });

    if(dia_mensaje.length === 1){//hay un dia registrado
        id_dia = dia_mensaje[0]._id;
        console.log(id_dia);
    }else{//no hay dia registrado
        const dia_mensaje_registrar = new DiaMensaje({
            productor: productor,
            dia:dia
        });
        await dia_mensaje_registrar.save();
        dia_mensaje = await DiaMensaje.find({productor:productor, dia:dia});
        id_dia = dia_mensaje[0]._id;
        console.log('en else: '+id_dia);
    }
    id_dia = mongoose.Types.ObjectId(id_dia);
    /**
     * guardar en mongo un dia y el nombre del productor que envio el msj como cabecera...
     * si ya existe el dia en cuestion con el mismo productor, entonces no guardar y obtener el id
     */
    //obtener el mensaje a enviar...
    var texto_mensaje = fs.readFileSync(__dirname+'/../services/archivo_mensaje.txt', 'utf-8');
    for(var i=0; i<datos_clientes.length; i++){

        //verificar que no se haya mandado ya a ese numero hoy...
        //ver en colecciones de mongo...
        var correcto='NO permitido';
        if(datos_clientes[i].permitido){ //entra cuando el usuario permitio mandar este numero
            //se busca coincidir dia, poliza y telefono
            var dias_auxs = await DiaMensaje.aggregate(
                [{ "$match": { _id:id_dia} },
                { "$unwind": "$datos_dia_mensaje" },
                {
                    "$match": 
                        {
                            "datos_dia_mensaje.dia": dia,
                            "datos_dia_mensaje.poliza": String(datos_clientes[i].poliza),
                            "datos_dia_mensaje.telefono": datos_clientes[i].telefono,
                            "datos_dia_mensaje.nombre": datos_clientes[i].nombre,
                        }
                }] 
            );
            console.log(dias_auxs);
            var habilitar = true;
            for(var w = 0; w < dias_auxs.length; w++){
                console.log(dias_auxs[w]);
                if(dias_auxs[w].datos_dia_mensaje.correcto=='1' && String(dias_auxs[w].datos_dia_mensaje.texto_enviado)==String(texto_mensaje)){//y que el msj sea el mismo...
                    habilitar = false;
                    correcto='Error! Mensaje enviado en esta misma fecha.';
                    retorno_resultado_proceso.misma_fecha +=1;
                }
            }

            if(habilitar){//entra solo si no se mando en el mismo dia.. despues hacer dentro de los dos dias sgtes.
                correcto = ((await mandar_whatsapp(datos_clientes[i].telefono)).resultado);
                if(String(correcto)=='1'){
                    retorno_resultado_proceso.enviado +=1;
                }
                if(String(correcto)=='0'){
                    retorno_resultado_proceso.no_tiene_wpp +=1;
                }
            }
        }else{
            if(datos_clientes[i].telefono='0'){
                retorno_resultado_proceso.telefono_vacio +=1; 
            }else{
                retorno_resultado_proceso.con_telefono_deshabilitado +=1;
            }
            
        }
        
        valores = new Object({
            nombre:datos_clientes[i].nombre,
            poliza:String(datos_clientes[i].poliza),
            telefono: datos_clientes[i].telefono,
            correcto: String(correcto),
            dia: dia,
            fecha_hora: moment().format('MMMM Do YYYY, h:mm:ss a'),
            permitido: datos_clientes[i].permitido,
            texto_enviado: texto_mensaje
        });
       
        await DiaMensaje.updateOne(
            {_id:id_dia},
            {
                $push:
                    {
                        datos_dia_mensaje:valores                        
                    }
            }
        );


        errores[errores.length]= valores;
        //guardar en mongo para no repetir
    }
    console.log('Mensajes enviados correctamente ');
    console.log(errores);
    console.log(retorno_resultado_proceso);
    // res.json(
    //     {
    //         status:'Mensajes enviados correctamente ',
    //         resultados: errores
    //     }
    // );
    res.json(retorno_resultado_proceso);



    
    //return res.json('okidoki');
};



function mandar_whatsapp(parametro){
    var headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
        'Content-Type' : 'ext/html; charset=UTF-8'
    }
    var options = {
        url: 'http://localhost:5000/whatsapp/'+parametro,
        method: 'GET',
        headers: headers
       
    }
    return new Promise(function (resolve, reject){
        var resultado='asd';
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                
                resolve({resultado: JSON.parse(response.body).error});
                console.log(JSON.parse(response.body).error);
            }
            
        })

    });
}

whatController.obtener_mensaje_envio = async (req, res) => {
    var mensaje = fs.readFileSync(__dirname+'/../services/archivo_mensaje.txt', 'utf-8');
    return res.json(mensaje);
};
whatController.setear_mensaje_envio = async (req, res) => {
    console.log('mensaje');
    console.log(req.body);
    var mensaje = fs.writeFileSync(__dirname+'/../services/archivo_mensaje.txt',req.body.mensaje ,'utf-8');
    return res.json('OK');
};
module.exports = whatController;